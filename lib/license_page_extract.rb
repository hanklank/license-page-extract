require "license_page_extract/version"
require 'net/http'

module LicensePageExtract
  class LicensePageExtract

    URI_OPEN_SOURCE_ORG = "https://opensource.org/licenses/"
    URI_CHOOSE_LICENSE_RAW =  'https://raw.githubusercontent.com/github/choosealicense.com/gh-pages/_licenses/'

    def self.getLicenseText(metadata = true, uri = "")
      if uri.include? URI_OPEN_SOURCE_ORG
        licensefile = uri.split('/')[-1].downcase + ".txt"
        uri = URI("#{URI_CHOOSE_LICENSE_RAW}#{licensefile}")
        license = Net::HTTP.get(uri) 
        if !to_boolean(metadata)
          license = license.gsub(/---.*---/m,'')
        end
        puts "license was " + license
        return license
      end
    end

    def self.to_boolean(string)
      case string
      when /^(true|t|yes|y|1)$/i then true
      when /^(false|f|no|n|0)$/i then false
      else raise "Cannot convert to boolean: #{string}"
      end
    end

  end
end
